# # Utiliser une image PHP avec Apache
# FROM php:8.2-apache

# # Installer les dépendances nécessaires pour MySQL

# ADD --chmod=0755 https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

# RUN install-php-extensions pdo_mysql mysqli intl

# RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls &&  \
#     mv composer.phar /usr/local/bin/composer

# RUN apt update && apt install -yqq nodejs npm

# # Copier les fichiers de l'application Symfony dans le conteneur
# COPY . /var/www/html

# RUN cd  /var/www/html && \
#     composer install --no-interaction && \
#     npm install --force && \
#     npm run build

# # Exposer le port 80
# EXPOSE 80
