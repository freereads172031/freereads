<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Faker\Guesser\Name;
use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:create-admin',
    description: 'Create new admin user',
)]

class CreateAdminService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
    )
    {
        
    }

    public function create(string $email, string $password): void
    {
        // Create admin user
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if (!$user) {

            $user = new User();
            $user->setEmail($email);
    
            $password = $this->passwordHasher->hashPassword($user, $password);
            $user->setPassword($password);
        }

        $user->setRoles(['ROLE_ADMIN']);

        $this->userRepository->save($user);
    }
}