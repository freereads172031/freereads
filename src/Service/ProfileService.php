<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\User;
use App\Entity\UserBook;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use App\Repository\PublisherRepository;
use App\Repository\UserBookRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProfileService
{
    public function __construct(
        private GoogleBooksApiService $googleBooksApiService,
        private BookRepository $bookRepository,
        private AuthorRepository $authorRepository,
        private PublisherRepository $publisherRepository,
        private UserBookRepository $userBookRepository
    ) {
    }

    public function addBookToProfile(User $user, string $id): UserBook
    {
        // Récupérer le livre depuis Google Books API ou créer s'il n'existe pas encore
        $book = $this->getOrCreateBook($id);

        // Verifier si l user n'a pas déja  ce livre dans sa liste de lecture
        $userBook = $this->userBookRepository->findOneBy([
            'reader' => $user,
            'book' => $book
        ]);

        if (!$userBook)
        {
            $userBook = new UserBook();

            $userBook
                ->setReader($user)
                ->setBook($book)
                ->setCreatedAt(new \DateTimeImmutable());

            $this->userBookRepository->save($userBook, true);
        }

        return $userBook;
    }

    private function getOrCreateBook(string $id): Book
    {
        $bookFromGoogle = $this->googleBooksApiService->get($id);

        // Vérifier si le livre existe déjà dans la base de données
        $book = $this->bookRepository->findOneBy([
            'googleBooksId' => $id,
        ]);

        if (!$book) {
            // si le livre n'existe pas dans la base de données, donc créons-en un nouveau
            $book = new Book();

            $book
                ->setTitle($bookFromGoogle['volumeInfo']['title'] ?? null)
                ->setSubtitle($bookFromGoogle['volumeInfo']['subtitle'] ?? null)
                ->setDescription($bookFromGoogle['volumeInfo']['description'] ?? null)
                ->setGoogleBooksId($id)
                ->setIsbn10($bookFromGoogle['volumeInfo']['industryIdentifiers'][0]['identifier'] ?? null)
                ->setIsbn13($bookFromGoogle['volumeInfo']['industryIdentifiers'][1]['identifier'] ?? null)
                ->setPageCount($bookFromGoogle['volumeInfo']['pageCount'] ?? null)
                ->setPublishedDate(new \DateTimeImmutable($bookFromGoogle['volumeInfo']['publishedDate'] ?? null))
                ->setSmallThumbnail($bookFromGoogle['volumeInfo']['imageLinks']['smallThumbnail'] ?? null)
                ->setThumbnail($bookFromGoogle['volumeInfo']['imageLinks']['thumbnail'] ?? null);

            // Ajouter les auteurs du livre
            foreach ($bookFromGoogle['volumeInfo']['authors'] ?? [] as $authorName) {
                $author = $this->getOrCreateAuthor($authorName);
                $book->addAuthor($author, true);
            }
            // Ajouter le publisher du livre
            $publisher = $this->getOrCreatePublisher($bookFromGoogle['volumeInfo']['publisher']);
            $book->addPublisher($publisher);

            // Enregistrer le nouveau livre dans la base de données
            $this->bookRepository->save($book, true);
        }
        return $book;
    }

    private function getOrCreateAuthor(string $name): Author
    {
        $author = $this->authorRepository->findOneBy([
            'name' => $name
        ]);

        if (!$author) {
            // si l'auteur n'existe pas dans la base de données, donc créons-en un nouveau
            $author = new Author();
            $author->setName($name);
            $this->authorRepository->save($author, true);
        }

        return $author;
    }

    private function getOrCreatePublisher(string $name): Publisher
    {
        $publisher = $this->publisherRepository->findOneBy([
            'name' => $name
        ]);

        if (!$publisher) {
            // si l'éditeur n'existe pas dans la base de données, donc créons-en un nouveau
            $publisher = new Publisher();
            $publisher->setName($name);
            $this->publisherRepository->save($publisher, true);
        }

        return $publisher;
    }
}
