/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉')
// Bootstrap
import './styles/app.css';

import 'bootstrap';

import 'htmx.org';
window.htmx = require('htmx.org');


