<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\User;
use App\Entity\UserBook;
use App\Entity\Status;
use PHPUnit\Framework\TestCase;

class UserBookTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $userBook = new UserBook();

        $createdAt = new \DateTimeImmutable();
        $updatedAt = new \DateTimeImmutable();
        $comment = 'This is a comment';
        $rating = 5;
        $reader = new User();
        $book = new Book();
        $status = new Status();

        $userBook->setCreatedAt($createdAt);
        $userBook->setUpdatedAt($updatedAt);
        $userBook->setComment($comment);
        $userBook->setRating($rating);
        $userBook->setReader($reader);
        $userBook->setBook($book);
        $userBook->setStatus($status);

        $this->assertSame($createdAt, $userBook->getCreatedAt());
        $this->assertSame($updatedAt, $userBook->getUpdatedAt());
        $this->assertSame($comment, $userBook->getComment());
        $this->assertSame($rating, $userBook->getRating());
        $this->assertSame($reader, $userBook->getReader());
        $this->assertSame($book, $userBook->getBook());
        $this->assertSame($status, $userBook->getStatus());
    }
}