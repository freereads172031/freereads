<?php

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testGetUserBooks(): void
    {
        $status = new Status();
        $userBook1 = new UserBook();
        $userBook2 = new UserBook();

        $status->addUserBook($userBook1);
        $status->addUserBook($userBook2);

        $userBooks = $status->getUserBooks();

        $this->assertInstanceOf(\Doctrine\Common\Collections\Collection::class, $userBooks);
        $this->assertCount(2, $userBooks);
        $this->assertTrue($userBooks->contains($userBook1));
        $this->assertTrue($userBooks->contains($userBook2));
    }

    public function testAddUserBook(): void
    {
        $status = new Status();
        $userBook = new UserBook();

        $status->addUserBook($userBook);

        $userBooks = $status->getUserBooks();

        $this->assertCount(1, $userBooks);
        $this->assertTrue($userBooks->contains($userBook));
        $this->assertSame($status, $userBook->getStatus());
    }

    public function testRemoveUserBook(): void
    {
        $status = new Status();
        $userBook = new UserBook();

        $status->addUserBook($userBook);
        $status->removeUserBook($userBook);

        $userBooks = $status->getUserBooks();

        $this->assertCount(0, $userBooks);
        $this->assertFalse($userBooks->contains($userBook));
        $this->assertNull($userBook->getStatus());
    }
}