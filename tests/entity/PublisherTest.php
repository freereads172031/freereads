<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testGetBooks(): void
    {
        $publisher = new Publisher();
        $book1 = new Book();
        $book2 = new Book();

        $publisher->addBook($book1);
        $publisher->addBook($book2);

        $books = $publisher->getBooks();

        $this->assertInstanceOf(\Doctrine\Common\Collections\Collection::class, $books);
        $this->assertCount(2, $books);
        $this->assertTrue($books->contains($book1));
        $this->assertTrue($books->contains($book2));
    }

    public function testAddBook(): void
    {
        $publisher = new Publisher();
        $book = new Book();

        $publisher->addBook($book);

        $books = $publisher->getBooks();

        $this->assertCount(1, $books);
        $this->assertTrue($books->contains($book));
        $this->assertTrue($book->getPublishers()->contains($publisher));
    }

    public function testRemoveBook(): void
    {
        $publisher = new Publisher();
        $book = new Book();

        $publisher->addBook($book);
        $publisher->removeBook($book);

        $books = $publisher->getBooks();

        $this->assertCount(0, $books);
        $this->assertFalse($books->contains($book));
        $this->assertFalse($book->getPublishers()->contains($publisher));
    }
}