<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testGetBooks(): void
    {
        $author = new Author();
        $book1 = new Book();
        $book2 = new Book();

        $author->addBook($book1);
        $author->addBook($book2);

        $books = $author->getBooks();

        $this->assertInstanceOf(\Doctrine\Common\Collections\Collection::class, $books);
        $this->assertCount(2, $books);
        $this->assertTrue($books->contains($book1));
        $this->assertTrue($books->contains($book2));
    }

    public function testAddBook(): void
    {
        $author = new Author();
        $book = new Book();

        $author->addBook($book);

        $books = $author->getBooks();

        $this->assertCount(1, $books);
        $this->assertTrue($books->contains($book));
        $this->assertTrue($book->getAuthors()->contains($author));
    }

    public function testRemoveBook(): void
    {
        $author = new Author();
        $book = new Book();

        $author->addBook($book);
        $author->removeBook($book);

        $books = $author->getBooks();

        $this->assertCount(0, $books);
        $this->assertFalse($books->contains($book));
        $this->assertFalse($book->getAuthors()->contains($author));
    }
}