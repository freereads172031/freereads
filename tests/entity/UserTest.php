<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testAddUserBook(): void
    {
        $user = new User();
        $userBook = new UserBook();

        $user->addUserBook($userBook);

        $userBooks = $user->getUserBooks();

        $this->assertCount(1, $userBooks);
        $this->assertTrue($userBooks->contains($userBook));
        $this->assertSame($user, $userBook->getReader());
    }

    public function testRemoveUserBook(): void
    {
        $user = new User();
        $userBook = new UserBook();

        $user->addUserBook($userBook);
        $user->removeUserBook($userBook);

        $userBooks = $user->getUserBooks();

        $this->assertCount(0, $userBooks);
        $this->assertFalse($userBooks->contains($userBook));
        $this->assertNull($userBook->getReader());
    }

    public function testGetId(): void
    {
        $user = new User();
        $this->assertNull($user->getId());
    }

    public function testGetEmail(): void
    {
        $user = new User();
        $email = 'test@example.com';
        $user->setEmail($email);
        $this->assertSame($email, $user->getEmail());
    }

    public function testGetRoles(): void
    {
        $user = new User();
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        $user->setRoles($roles);
        $this->assertSame($roles, $user->getRoles());
    }

    public function testGetPassword(): void
    {
        $user = new User();
        $password = 'password123';
        $user->setPassword($password);
        $this->assertSame($password, $user->getPassword());
    }

    public function testGetPseudo(): void
    {
        $user = new User();
        $pseudo = 'JohnDoe';
        $user->setPseudo($pseudo);
        $this->assertSame($pseudo, $user->getPseudo());
    }
}