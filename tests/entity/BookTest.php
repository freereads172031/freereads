<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testAddAuthor(): void
    {
        $book = new Book();
        $author = new Author();

        $book->addAuthor($author);

        $authors = $book->getAuthors();

        $this->assertCount(1, $authors);
        $this->assertTrue($authors->contains($author));
        $this->assertFalse($author->getBooks()->contains($book));
    }

    public function testRemoveAuthor(): void
    {
        $book = new Book();
        $author = new Author();

        $book->addAuthor($author);
        $book->removeAuthor($author);

        $authors = $book->getAuthors();

        $this->assertCount(0, $authors);
        $this->assertFalse($authors->contains($author));
        $this->assertFalse($author->getBooks()->contains($book));
    }

    public function testAddPublisher(): void
    {
        $book = new Book();
        $publisher = new Publisher();

        $book->addPublisher($publisher);

        $publishers = $book->getPublishers();

        $this->assertCount(1, $publishers);
        $this->assertTrue($publishers->contains($publisher));
        $this->assertFalse($publisher->getBooks()->contains($book));
    }

    public function testRemovePublisher(): void
    {
        $book = new Book();
        $publisher = new Publisher();

        $book->addPublisher($publisher);
        $book->removePublisher($publisher);

        $publishers = $book->getPublishers();

        $this->assertCount(0, $publishers);
        $this->assertFalse($publishers->contains($publisher));
        $this->assertFalse($publisher->getBooks()->contains($book));
    }

    public function testAddUserBook(): void
    {
        $book = new Book();
        $userBook = new UserBook();

        $book->addUserBook($userBook);

        $userBooks = $book->getUserBooks();

        $this->assertCount(1, $userBooks);
        $this->assertTrue($userBooks->contains($userBook));
        $this->assertSame($book, $userBook->getBook());
    }

    public function testRemoveUserBook(): void
    {
        $book = new Book();
        $userBook = new UserBook();

        $book->addUserBook($userBook);
        $book->removeUserBook($userBook);

        $userBooks = $book->getUserBooks();

        $this->assertCount(0, $userBooks);
        $this->assertFalse($userBooks->contains($userBook));
        $this->assertNull($userBook->getBook());
    }
}